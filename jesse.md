# Jesse Pye
A little about me: I graduated from University of Texas at Austin with a degree in math and worked for a few years as a math and science instructor.
At the beginning of 2021, I graduated from the Hack Reactor coding bootcamp, and soon after I was accepted into the vmware ascent training program.
During Hack Reactor, I created (among other things) a user authentication system with a RESTful API served by an Express back end and a Postgres database; during the vmware ascent bootcamp my team and I implemented an ElasticSearch microservice which indexed the contents of the social media website written by our bootcamp classmates.
Hobbies include camping and hiking (just took a vacation to Palo Duro, the "grand canyon of texas" ) and hanging out at parks and swimming holes with our friends around Austin.
